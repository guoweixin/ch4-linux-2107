package com.qfjy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch4LinuxApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch4LinuxApplication.class, args);
    }

}
